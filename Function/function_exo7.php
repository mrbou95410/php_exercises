<?php
function retourner($age, $genre){
    if($age>18 && $genre=="Homme"){
        return "Vous êtes un homme et vous êtes majeur";
    }elseif($age<18 && $genre=="Homme"){
        return "Vous êtes un homme et vous êtes mineur";    
    }elseif($age>18 && $genre=="Femme"){
        return "Vous êtes une femme et vous êtes majeur";
    }else{
        return "Vous êtes une femme et vous êtes mineur";
    }

}
echo retourner(24, "Homme"). "\n";
echo retourner(14, "Homme"). "\n";
echo retourner(20, "Femme"). "\n";
echo retourner(14, "Femme"). "\n";
?>