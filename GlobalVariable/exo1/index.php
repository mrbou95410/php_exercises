<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice SuperGlobal</title>
</head>
<body>
<p> 
    <?php
        echo "Votre User Agent est : " . $_SERVER['HTTP_USER_AGENT'] . " ";
        echo "Votre adresse IP est : " . $_SERVER['REMOTE_ADDR'] . "\n";
        echo "Le nom de votre serveur est: " . $_SERVER['SERVER_NAME'];

    ?>
</body>
</html>